# Requires GNU Make 4.3:
#.RECIPEPREFIX +=

# trailing space needed!
.RECIPEPREFIX := $(.RECIPEPREFIX)

CFLAGS = -O2 -lev

camera-splitter: camera-splitter.c

all: camera-splitter

	#gcc -I /opt/local/include -o camera-splitter camera-splitter.c -L /opt/local/lib -lev
	#gcc -o camera-splitter camera-splitter.c -lev
