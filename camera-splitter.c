/* for memmem: */
#define _GNU_SOURCE         /* See feature_test_macros(7) */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <assert.h>
#include <errno.h>
#include <stdbool.h>

#include <string.h>

#ifdef __linux__
#include <sys/prctl.h>
#endif

#include <arpa/inet.h>
#include <netinet/in.h>

//#include <uv.h>
#include <ev.h>

#define MY_IP_PORT 5001

#define CROAK exit(1)

#define DEBUG 0
#define PRINTSTATS 0

#define STATS_PERIOD 10.

#define LOG(...) do { \
    fprintf(stderr, __VA_ARGS__); \
    fprintf(stderr, "\n"); \
} while (0)

#define DODEBUG(...) if (DEBUG) { \
    fprintf(stderr, "DEBUG (line %d): ", __LINE__); \
    fprintf(stderr, __VA_ARGS__); \
    fprintf(stderr, "\n"); \
}

#define NAL_START "\0\0\0\1"
#define NAL_START_LENGTH 4

#define SIGNAL_TO_KILL_CAMERA SIGHUP

#define SOURCE_IS_ACTIVE(ctx_p) (ctx_p->childproc.pid != 0)

#define READBUF_SIZE 65536
#define HEADERBUF_SIZE READBUF_SIZE

#define ERRNO_MEANS_UNAVAILABLE (errno == EAGAIN || errno == EWOULDBLOCK)

#define RESIZE_ARRAY(SRC, COUNT) \
    SRC = realloc( SRC, COUNT * sizeof(*SRC) )

#ifndef ev_io_modify
#define ev_io_modify(w, evts) ev_io_set(w, w->fd, evts)
#endif

static char* const command[] = {
/*
    "/usr/bin/perl",
    "-E",
    "$| = 1; say( time % 5 ) && sleep 1 while 1",
*/
    "/usr/bin/raspivid",
    "--width", "1280",
    "--height", "720",
    "--rotation", "180",
    "--timeout", "0",
    "--nopreview",
    "--output", "-",
    NULL
};

typedef struct {
    ev_io watcher;
    pid_t pid;
#if PRINTSTATS
    ev_timer timer;
    ev_tstamp lasttime;
    uint32_t bytessent;
#endif
} childproc_t;

struct context_s;

typedef struct {
    struct context_s* context_p;
    ev_io* watcher_p;
} connection_t;

typedef struct context_s {
    struct ev_loop *loop_p;

    connection_t** client_connections;
    unsigned client_count;

    childproc_t childproc;

    unsigned char readbuf[READBUF_SIZE];
    ssize_t readbuf_size;

    unsigned char header[HEADERBUF_SIZE];
    ssize_t header_size;
} context_t;

typedef int fd_t;

/* ---------------------------------------------------------------------- */

static void spawn(context_t* context_p);

/* ---------------------------------------------------------------------- */

static void _safe_close_socket(fd_t fd) {
    shutdown(fd, SHUT_RD);

    unsigned char dummy[65536];
    while (read(fd, dummy, 65536));

    close(fd);
}

static void _make_nonblocking(fd_t fd) {
    if (-1 == fcntl(fd, F_SETFL, O_NONBLOCK)) {
        perror("fcntl F_SETFL:O_NONBLOCK");
        CROAK;
    }
}

static void _make_cloexec(fd_t fd) {
    if (-1 == fcntl(fd, F_SETFD, FD_CLOEXEC)) {
        perror("fcntl F_SETFD:FD_CLOEXEC");
        CROAK;
    }
}

static bool
_set_header( context_t* context_p, fd_t fd ) {
    DODEBUG("setting header");

    ssize_t got = recv( fd, context_p->header, HEADERBUF_SIZE, MSG_PEEK );

    if (-1 == got) {
        perror("recv");
        CROAK;
    }

    DODEBUG("setting header (%d bytes read)", got);

    unsigned char* nal_at = memmem( 1 + context_p->header, got - 1, NAL_START, NAL_START_LENGTH);

    if (NULL != nal_at) {
        unsigned nal_offset = nal_at - context_p->header;

        DODEBUG("setting header - mark 1 (NAL2 at +%d bytes)", nal_offset);

        /* Search again … */
        nal_at = memmem( 1 + nal_at, got - nal_offset - 1, NAL_START, NAL_START_LENGTH);
    }

    if (NULL == nal_at) {
        DODEBUG("setting header - no dice");

        if (context_p->header_size == HEADERBUF_SIZE) {
            fprintf(stderr, "No header within %d bytes from camera?!?\n", HEADERBUF_SIZE);
            CROAK;
        }

        return false;
    }

    DODEBUG("setting header - success");

    context_p->header_size = nal_at - context_p->header;

    return true;
}

static void
child_read_cb (EV_P_ ev_io *w, int revents) {
    DODEBUG("child read callback");

    context_t* context_p = w->data;

    unsigned client_count = context_p->client_count;

    assert( client_count != 0 );

    if (!context_p->header_size) {
        if (!_set_header(context_p, w->fd)) return;
    }

    connection_t** connections_p = context_p->client_connections;

    /*
        Given clients A, B, and C.
        Given 10 bytes to write.

        Assume client A writes all 10 bytes.

        Assume client B writes only 8 bytes. The last 2 bytes are a
        “deficit” for B. Also disable the reader. Also enable B’s
        write watcher. Also save the buffer. Also increase the
        context’s write count.

        Assume client C writes only 5 bytes. Set the last 5 bytes
        as C’s deficit, and enable C’s write watcher.
    */

    DODEBUG("reading to %p (%d bytes)", context_p->readbuf, READBUF_SIZE);

    context_p->readbuf_size = read( w->fd, context_p->readbuf, READBUF_SIZE );

    DODEBUG("did read (%d bytes)", context_p->readbuf_size);

#if PRINTSTATS
    context_p->childproc.bytessent += context_p->readbuf_size;
#endif

    if (context_p->readbuf_size == -1) {
        if (ERRNO_MEANS_UNAVAILABLE) return;

        perror("read from camera");
        CROAK;
    }

    if (context_p->readbuf_size == 0) {
        fprintf(stderr, "camera stopped writing??");
        CROAK;
    }

    for (unsigned i = 0; i<client_count; i++) {
        ssize_t sent = send(
            connections_p[i]->watcher_p->fd,
            context_p->readbuf,
            context_p->readbuf_size,
            0
        );

        if (-1 == sent) {
            if (ERRNO_MEANS_UNAVAILABLE) {
                sent = 0;
            }
            else {
                perror("send to client");
                CROAK;
            }
        }
    }
}

static void
_forget_client(ev_io *w) {
    _safe_close_socket(w->fd);

    connection_t* connection_p = w->data;
    context_t* context_p = connection_p->context_p;

    ev_io_stop(context_p->loop_p, w);

    context_p->client_count--;
    LOG("Remaining connections: %d", context_p->client_count);

    connection_t** old_connections_p = context_p->client_connections;

    if (context_p->client_count) {
        connection_t** new_connections_p = malloc( context_p->client_count * sizeof(connection_t*));

        unsigned curnew = 0;

        for (unsigned i=0; i<=context_p->client_count; i++) {
            if (old_connections_p[i] != connection_p) {
                new_connections_p[curnew++] = old_connections_p[i];
            }
        }

        context_p->client_connections = new_connections_p;
    }
    else {
        LOG("No more connections; ending subprocess %d", context_p->childproc.pid);
        close(context_p->childproc.watcher.fd);

        ev_io_stop(context_p->loop_p, &context_p->childproc.watcher);

#if PRINTSTATS
        ev_timer_stop(context_p->loop_p, &context_p->childproc.timer);
#endif

        // Shouldn’t be freed because it’s part of the struct.
        //free(&context_p->childproc.watcher);

        kill(SIGNAL_TO_KILL_CAMERA, context_p->childproc.pid);
        context_p->childproc.pid = 0;

        context_p->client_connections = NULL;
    }

DODEBUG("freeing old connections list %p", old_connections_p);
    free(old_connections_p);
DODEBUG("freed old connections list");

    // Free the connection_t
DODEBUG("freeing old connection %p");
    free(connection_p);
DODEBUG("freed old connection");

    // Free the watcher
DODEBUG("freeing watcher %p", w);
    free(w);
DODEBUG("freed watcher");
}

static void
connection_cb (EV_P_ ev_io *w, int revents) {
    DODEBUG("XXXXXXXXXX connection callback");

    /*
        We’re here because previously w->fd had an incomplete write.
    */

    if (revents & EV_READ) {
        DODEBUG("connection readable");
        unsigned char foo;

        ssize_t got = read(w->fd, &foo, 1);

        if (got > 0) {
            fprintf(stderr, "Unexpected input from client; dropping.\n");
        }
        else if (got == 0) {
            fprintf(stderr, "Client dropped; removing.\n");
        }
        else {
            perror("Failed to read from client (dropping)");
        }

        _forget_client(w);
    }

    return;
}

#if PRINTSTATS
static void
timer_cb (EV_P_ ev_timer *w, int revents) {
    childproc_t* childproc_p = w->data;

    ev_tstamp lasttime = childproc_p->lasttime;
    ev_tstamp now = ev_now(loop);

    LOG(
        "%lu bytes sent in %.02f seconds (%.02f KiB/s)",
        childproc_p->bytessent,
        now - lasttime,
        (childproc_p->bytessent / (now - lasttime)) / 1024
    );

    childproc_p->bytessent = 0;
    childproc_p->lasttime = now;
}
#endif

static void
srv_cb (EV_P_ ev_io *w, int revents) {
    DODEBUG("server socket callback");

    context_t* context_p = w->data;

    struct sockaddr_in address;
    socklen_t address_len = sizeof(address);

    fd_t connfd = accept( w->fd, (struct sockaddr *) &address, &address_len );

    if (-1 == connfd) {
        if (errno == EWOULDBLOCK) return;

        perror("accept");
        CROAK;
    }

    DODEBUG("accepted (FD: %d)", connfd);

    LOG(
        "Got connection (#%d) from %s:%d",
        1 + context_p->client_count,
        inet_ntoa( address.sin_addr ),
        ntohs(address.sin_port)
    );

    //_make_nonblocking(connfd);
    _make_cloexec(connfd);

    context_p->client_count++;

    context_p->client_connections = realloc(
        context_p->client_connections,
        context_p->client_count * sizeof(connection_t*)
    );

    connection_t* connection_p = malloc( sizeof(connection_t) );
    connection_p->context_p = context_p;

    context_p->client_connections[ context_p->client_count - 1 ] = connection_p;

    ev_io* watcher_p = malloc( sizeof(ev_io) );
    ev_io_init(
        watcher_p,
        connection_cb,
        connfd,
        EV_READ
    );

    ev_io_start(context_p->loop_p, watcher_p);

    // Circular reference … hmm …
    watcher_p->data = connection_p;
    connection_p->watcher_p = watcher_p;

    if (!SOURCE_IS_ACTIVE(context_p)) {
        spawn(context_p);
    }
    else if (context_p->header_size) {
        LOG("New connection is secondary; sending stored header");

        ssize_t sent = write(connfd, context_p->header, context_p->header_size);

        if (-1 == sent) {
            perror("write header to new client");
            CROAK;
        }
    }
}

fd_t start_srv() {
    fd_t srvfd = socket(AF_INET, SOCK_STREAM, 0);
    if (-1 == srvfd) {
        perror("socket");
        CROAK;
    }

    // SO_DONTLINGER on win32, NB:
    setsockopt(srvfd, SOL_SOCKET, SO_REUSEADDR, &(int){1}, sizeof(int));

    struct sockaddr_in my_addr;
    memset(&my_addr, 0, sizeof(my_addr));

    my_addr.sin_family = AF_INET;
    my_addr.sin_port = htons(MY_IP_PORT);
    my_addr.sin_addr.s_addr = INADDR_ANY;

    if (-1 == bind(srvfd, (struct sockaddr *) &my_addr, sizeof(struct sockaddr_in))) {
        perror("bind");
        CROAK;
    }

    LOG(
        "Server listening on %s:%d",
        inet_ntoa( my_addr.sin_addr ),
        ntohs(my_addr.sin_port)
    );

    if (-1 == listen(srvfd, 5)) {
        perror("listen");
        CROAK;
    }

    _make_nonblocking(srvfd);
    _make_cloexec(srvfd);

    return srvfd;
}

/*
uv_process_t spawn(uv_loop_t* loop) {
    uv_process_options_t options;
    uv_process_t child_req;

    options.file = command[0];
    options.args = command;
    options.exit_cb = NULL;

    int r;
    if ((r = uv_spawn(loop, &child_req, &options))) {
        fprintf(stderr, "%s\n", uv_strerror(r));
        return 1;
    }
    else {
        fprintf(stderr, "Launched process with ID %d\n", child_req.pid);
    }

    return child_req;
}
*/

static void spawn(context_t* context_p) {
    DODEBUG("in spawn()");

    int rdwr[2];

    if (-1 == socketpair(AF_UNIX, SOCK_STREAM, 0, rdwr)) {
        perror("socketpair");
        CROAK;
    }

    _make_nonblocking(rdwr[0]);
    _make_nonblocking(rdwr[1]);

    /* ONLY the reader! */
    _make_cloexec(rdwr[0]);

    pid_t childpid = fork();

    if (-1 == childpid) {
        perror("fork");
        CROAK;
    }

    if (childpid) {
        LOG("%s starting (PID %d)", command[0], childpid);

        close(rdwr[1]);

        childproc_t* childproc_p = &context_p->childproc;

        childproc_p->pid = childpid;

        DODEBUG("initing child watcher");

        childproc_p->watcher.data = context_p;

        ev_io_init (&childproc_p->watcher, child_read_cb, rdwr[0], EV_READ);
        ev_io_start(context_p->loop_p, &childproc_p->watcher);

#if PRINTSTATS
        childproc_p->lasttime = ev_now(context_p->loop_p);
        childproc_p->bytessent = 0;
        childproc_p->timer.data = childproc_p;
        ev_timer_init(&childproc_p->timer, timer_cb, STATS_PERIOD, STATS_PERIOD);
        ev_timer_start(context_p->loop_p, &childproc_p->timer);
#endif

        DODEBUG("did init child watcher");

        /*
        uv_pipe_t uvpipe;
        uv_pipe_init(loop, &uvpipe, true);
        uv_pipe_open(&uvpipe, rdwr[0]);
        */

        return;
    }

    // Child process:

    // Linux: ensures the child exits if the parent does
#ifdef __linux__
    prctl(PR_SET_PDEATHSIG, SIGNAL_TO_KILL_CAMERA);
#endif

    close(rdwr[0]);

    if ( -1 == dup2(rdwr[1], fileno(stdout)) ) {
        perror("dup2");
        CROAK;
    }

    execv( command[0], command );
}

/* ---------------------------------------------------------------------- */

int main() {
    fclose(stdin);

    fd_t srvfd = start_srv();

    struct ev_loop *loop_p = EV_DEFAULT;

    context_t context;

    context.loop_p = loop_p;
    context.readbuf_size = 0;
    context.header_size = 0;
    context.client_count = 0;
    context.client_connections = NULL;
    context.childproc.pid = 0;

    ev_io srv_watcher;
    ev_io_init(&srv_watcher, srv_cb, srvfd, EV_READ);

    srv_watcher.data = &context;

    ev_io_start(loop_p, &srv_watcher);

    //----------------------------------------------------------------------

    DODEBUG("Listening (nc -vvv localhost %d) …", MY_IP_PORT);

    ev_run(loop_p, 0);

    return 0;
}
